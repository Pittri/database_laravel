<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;


class profits extends Model
{
    use HasFactory;
     /**
     * fillable
     * 
     * @var array
     */
    protected $fillable = [
        'Transaction_id','total'
    ];
/**
 * Transactions
 * 
 * @return void
 */
public function Transactions ()
{
       return $this->belongsTo(Transactions::class);
}

/**
 * createdAt
 * 
 * @return attribute
 */
protected function createdAd(): Attribute
{
    return Attribute::make(
        get: fn ($value) => Carbon:: perse($value)->format('d-M-Y H:i:s'),
    );
}
}
