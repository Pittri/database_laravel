<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;


class transactions extends Model
{
    use HasFactory;
    /**
     * fillable
     * 
     * @var array
     */
    protected $fillable = [
        'invoice','cash','change','discount','grand_total'
    ];
/**
 * Transactions_details
 * 
 * @return void
 */
public function Transactions_details()
{
    return $this->hasMany(Transactions_details::class);
}

/**
 * Customer
 * 
 * @return void
 */
public function Customer()
{
    return $this->belongsTo(Customer::class);
}

/**
 * cashier
 * 
 * @return void
 */
public function Cashier()
{
    return $this->belongsTo(Users::class, 'Cashier_id');
}

/**
 * profits
 * 
 * @return void
 */
public function Profits()
{
    return $this->hasMany(Profits::class);
}

/**
 * createdSt
 * 
 * @return attribute
 */
protected function createdAt(): Attribute
{
    return Attribute::make(
        get: fn ($value) => Carbon::perse($value)->format('d-M-Y H:i:s'),
    );
}
}
