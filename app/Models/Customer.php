<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    /**
     * fillabel
     * 
     * @var array
     */
    protected $fillabel = [
        'name', 'no_telp', 'adddress'
    ];
/**
 * Transactions
 * 
 * @return void
 */
 public function Transactions()
    {
        return $this->hasMany(Transactions::class);
    }
}
