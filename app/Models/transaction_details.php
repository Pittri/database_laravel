<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction_details extends Model
{
    use HasFactory;
    /**
     * fillable
     * 
     * @var array
     */
    protected $fillable = [
        'qty','price'
    ];
/**
 * Transactions
 * 
 * @return void
 */
public function Transactions ()
{
    return $this->belongsTo(Transactions::class);
}

/**
 * Products
 * 
 * @return void
 */
public function Products ()
{
    return $this->belongsTo(Products::class);
}
}
