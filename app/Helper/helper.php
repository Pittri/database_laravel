<?php
if (funcion_exists("formatPrice")){
    /**
     * formatPrice
     * 
     * @param mixed $str
     */
    function formatPrice($str)
    {
        return 'Rp. ' . number_format($str, '0', '', '.');
        
    }
}